
This project has moved to https://github.com/mverleg/quora_unfade
==================================================================================

I am collecting all my projects in one place, which is Github, so development continues there!

Block 'read more' (Firefox addon)
---------------------------------------

Quora tries to force you into making an account by fading the page and putting an unclosable registration prompt in the middle of the page.

In case you don't like being forced to register, this simple addon is the alternative way to remove the prompt and fading.

Contributors
---------------------------------------

Bugfixes are welcome. The code is at https://bitbucket.org/mverleg/firefox_quora_unfade

License
---------------------------------------

Revised BSD License, see LICENSE.txt. You can use the addon as you choose, without warranty. The code is also available, see Contributors.

Other addons
---------------------------------------

* Hide distracting "read more" parts of some popular sites - https://addons.mozilla.org/firefox/addon/block_read_more/
* Hide comments on some popular sites where they are notoriously unconstructive - https://addons.mozilla.org/firefox/addon/block-comments/
* Hide the registration overlay on Quora [this one] - https://addons.mozilla.org/firefox/addon/quora-unfade/


